package com.techu.apitechu;

import com.techu.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/apitechu/v1")
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {
		SpringApplication.run(ApitechuApplication.class, args);
		ApitechuApplication.productModels= ApitechuApplication.getTestData();
	}

	private static ArrayList<ProductModel> getTestData() {
		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel("1","Producto 1", 10.0F)
		);
		productModels.add(
				new ProductModel("2","Producto 2", 10.0F)
		);

		return productModels;
	}
}
