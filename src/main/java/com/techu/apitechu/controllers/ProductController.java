package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.Exceptions.NotFoundException;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/apitechu/v1/products")
public class ProductController {

    @GetMapping
    public ArrayList<ProductModel> getProducts() {
        return ApitechuApplication.productModels;
    }

    @GetMapping("/{id}")
    public ProductModel getProductById(@PathVariable String id) throws NotFoundException {
        return ApitechuApplication.productModels.stream().filter(x -> x.getId().equals(id)).findFirst()
                .orElseThrow(() -> new NotFoundException("Product with id " + id + "not found"));
    }

    @PostMapping
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        ApitechuApplication.productModels.add(newProduct);
        return ApitechuApplication.productModels.get(ApitechuApplication.productModels.size()-1);
    }

    @PutMapping("/{id}")
    public ProductModel updateProduct(@PathVariable String id, @RequestBody ProductModel product) {
        ApitechuApplication.productModels.replaceAll(x -> x.getId().equals(id) ? product : x);
        return product;
    }

    @PatchMapping("/{id}")
    public ProductModel patchProduct(@PathVariable String id,
                                      @RequestBody ProductModel prod) throws NotFoundException {
        ProductModel product = ApitechuApplication.productModels.stream()
                .filter(x -> x.getId().equals(id)).findFirst()
                .orElseThrow(() -> new NotFoundException("Product with id " + id + "not found"));
            if (prod.getDesc()!=null) {
                product.setDesc(prod.getDesc());
            }
            if (prod.getPrice()!=null) {
                product.setPrice(prod.getPrice());
            }
            ApitechuApplication.productModels.replaceAll(x -> x.getId().equals(id) ? product : x);
            return product;
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable String id) {
        ApitechuApplication.productModels.removeIf(x -> x.getId().equals(id));
    }
}
